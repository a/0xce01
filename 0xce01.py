import asyncio
import websockets
import time
import json
import aiohttp
import config

start_time = time.time()


async def inform(text, session):
    await session.post(config.webhook, json={"content": text})
    print(text)


async def main():
    aiohttp_session = aiohttp.ClientSession()

    uri = "wss://uhr.ptb.de/time"
    async with websockets.connect(uri) as websocket:
        while True:
            # Calc equivalent of javascript's performance.now and sent it over
            perf_now = (time.time() - start_time) * 1000
            payload = json.dumps({"c": perf_now})
            await websocket.send(payload)

            # Receive time from server
            response = await websocket.recv()
            recv_time = json.loads(response)["s"] / 1000

            # Go through each available event
            for event in config.events.copy():
                print(f"\"{config.events[event]}\": {event - recv_time}s to go")
                # Delete events past max deviation point
                # In case we haven't posted them yet
                if recv_time > event - config.discord_rtt + config.deviation:
                    del config.events[event]
                # If it's event time, then give the news to the group
                elif recv_time > event - config.discord_rtt:
                    await inform(config.events[event], aiohttp_session)
                    del config.events[event]

            deviation = time.time() - recv_time
            print(f"Current time: {recv_time}, dev: {deviation}s")

            # Shut down when we run out of events
            if not config.events:
                print("No more events left.")
                break

            await asyncio.sleep(1)

    print("Shutting down.")
    await aiohttp_session.close()


asyncio.get_event_loop().run_until_complete(main())
